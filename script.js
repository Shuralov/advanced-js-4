let newStack = document.querySelector('.newList');
newStack.addEventListener('click', function () {
    new Stack();
});

class Stack {
    constructor() {
        this.wrap = document.createElement('div');
        this.wrap.classList.add('wrap');
        document.body.append(this.wrap);
        this.stack = document.createElement('div');
        this.stack.classList.add('stack');
        this.wrap.append(this.stack);

        this.sortBtn = document.createElement('button');
        this.sortBtn.classList.add('sort');
        this.sortBtn.innerText = 'Sort';

        this.wrap.prepend(this.sortBtn);

        this.sortBtn.addEventListener('click', function () {
        this.inputs = this.parentNode.querySelectorAll('input');
        this.set = this.parentNode.querySelectorAll('.card');

        this.arrSet = Array.prototype.slice.call(this.set);
        this.arrInput = [].slice.call(this.inputs).sort(function (a, b) {
                return a.value > b.value ? 1 : -1;
            });
            for (let r = 0; r < this.arrInput.length; r++) {
                this.arrSet[r].appendChild(this.arrInput[r]);
            }
        })

        this.addBtn = document.createElement('button');
        this.addBtn.classList.add('stack__add');
        this.addBtn.innerText = 'Add a card';
        this.wrap.append(this.addBtn);
        this.addBtn.addEventListener('click', function () {
            this.stack = this.parentNode.querySelector('.stack');
            new Card().appendTo(this.stack);
        })
    }
}


class Card {
    constructor() {
        this.item = document.createElement('div');
        this.item.classList.add('card');
        this.item.setAttribute('draggable', true);
        this.stack = document.querySelector('.stack');
        this.item.addEventListener('drop', () => {
            this.draggable = document.querySelector('.dragging');

            this.next = this.draggable.nextSibling;
            this.previous = this.draggable.previousSibling;

            if (this.item === this.stack.firstChild) {
                this.item.before(this.draggable)
                console.log('first')
            }
            if (this.draggable.nextSibling === this.next) {
                this.item.after(this.draggable);
            }
            if (this.draggable.previousSibling === this.previous) {
                this.item.before(this.draggable);
            }
        })
        this.item.addEventListener('dragover', function (e) {
            e.preventDefault();
        });
        this.item.addEventListener('dragstart', () => {
            this.item.classList.add('dragging')
        })
        this.item.addEventListener('dragend', () => {
            this.item.classList.remove('dragging')
        })
        this.input = document.createElement('input');
        this.input.defaultValue = "Enter a title for this card...";
        this.item.append(this.input);
        this.input.addEventListener('focus', function () {
            if  (this.value === "Enter a title for this card...") {
                this.value='';
            }
        })
    }
    appendTo(container) {
        container.append(this.item);
    }
}


